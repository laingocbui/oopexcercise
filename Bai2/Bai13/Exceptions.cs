﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bai13
{
    public class Exceptions : Exception
    {
        public class BirthDayException : Exception
        {
            public BirthDayException(string message) : base(message)
            {
            }
        }

        public class PhoneException : Exception
        {
            public PhoneException(string message) : base(message)
            {
            }
        }

        public class EmailException : Exception
        {
            public EmailException(string message) : base(message)
            {
            }
        }

        public class FullNameException : Exception
        {
            public FullNameException(string message) : base(message)
            {
            }
        }
    }
}
