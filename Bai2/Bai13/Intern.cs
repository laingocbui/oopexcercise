﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bai13
{
    internal class Intern : Employee
    {
      
        public string Majors { get; set; }
        public int Semester { get; set; }
        public string University_name { get; set; }

     
        public Intern(string id, string fullName, DateTime birthDay, string phone, string email, List<Certificate> certificates, string majors, int semester, string university_name)
            : base(id, fullName, birthDay, phone, email, certificates)
        {
            Majors = majors;
            Semester = semester;
            University_name = university_name;
            Employee_type = 2;
            Employee_count++;
        }

       
        public override void ShowInfo()
        {
            Console.WriteLine($"ID: {ID}");
            Console.WriteLine($"Full Name: {FullName}");
            Console.WriteLine($"Birth Day: {BirthDay.ToShortDateString()}");
            Console.WriteLine($"Phone: {Phone}");
            Console.WriteLine($"Email: {Email}");
            
            Console.WriteLine($"Employee Type: Intern");
            Console.WriteLine($"Majors: {Majors}");
            Console.WriteLine($"Semester: {Semester}");
            Console.WriteLine($"University Name: {University_name}");
            Console.WriteLine("=======================");
    
            foreach (Certificate certificate in Certificates)
            {
                Console.WriteLine($"Certificate ID: {certificate.CertificateID}");
                Console.WriteLine($"Certificate Name: {certificate.CertificateName}");
                Console.WriteLine($"Certificate Rank: {certificate.CertificateRank}");
                Console.WriteLine($"Certificate Date: {certificate.CertificateDate}");
                Console.WriteLine("=======================");
            }

        }
    }
}
