﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bai13
{
    public abstract class Employee
    {
        
        public string ID { get; set; }
        public string FullName { get; set; }
        public DateTime BirthDay { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public int Employee_type { get; set; }
        public int Employee_count { get; set; }
        public List<Certificate> Certificates { get; set; }

        public Employee(string id, string fullName, DateTime birthDay, string phone, string email, List<Certificate> certificates)
        {
            ID = id;
            FullName = fullName;
            BirthDay = birthDay;
            Phone = phone;
            Email = email;
            Employee_count = 0;
            Certificates = certificates;
        }

    
        public abstract void ShowInfo();
    }
}
