﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bai3
{
    public class ThiSinhKhoiA : ThiSinh
    {
        public List<string> MonThi { get; set; }

        public ThiSinhKhoiA()
        {
            MonThi = new List<string> { "Toán", "Lý", "Hóa" };
        }
    }
}
