﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bai13
{
    internal class Experience : Employee
    {
        
        public int ExpInYear { get; set; }
        public string ProSkill { get; set; }

       
        public Experience(string id, string fullName, DateTime birthDay, string phone, string email, List<Certificate> certificates, int expInYear, string proSkill)
            : base(id, fullName, birthDay, phone, email, certificates)
        {
            ExpInYear = expInYear;
            ProSkill = proSkill;
            Employee_type = 0;
            Employee_count++;
        }

        
        public override void ShowInfo()
        {
            Console.WriteLine($"ID: {ID}");
            Console.WriteLine($"Full Name: {FullName}");
            Console.WriteLine($"Birth Day: {BirthDay.ToShortDateString()}");
            Console.WriteLine($"Phone: {Phone}");
            Console.WriteLine($"Email: {Email}");
            Console.WriteLine($"Employee Type: Experience");
            Console.WriteLine($"Exp In Year: {ExpInYear}");
            Console.WriteLine($"Pro Skill: {ProSkill}");
            Console.WriteLine("=======================");

            foreach (Certificate certificate in Certificates)
            {
                Console.WriteLine($"Certificate ID: {certificate.CertificateID}");
                Console.WriteLine($"Certificate Name: {certificate.CertificateName}");
                Console.WriteLine($"Certificate Rank: {certificate.CertificateRank}");
                Console.WriteLine($"Certificate Date: {certificate.CertificateDate}");
                Console.WriteLine("=======================");
            }
        }
    }
}
