﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bai12
{
    internal class QLPTGT
    {
        private List<PhuongTienGiaoThong> danhSachPTGT;
        HashSet<int> a = new HashSet<int>();

        public QLPTGT()
        {
            danhSachPTGT = new List<PhuongTienGiaoThong>();
        }

        public void ThemPhuongTien(PhuongTienGiaoThong ptgt)
        {
            if (a.Contains(ptgt.ID))
            {
                Console.WriteLine("ID Already exist");
                return;
            }
            a.Add(ptgt.ID);
            danhSachPTGT.Add(ptgt);
            Console.WriteLine("Thêm phương tiện thành công.");
        }

        public void XoaPhuongTien(int id)
        {
            PhuongTienGiaoThong ptgt = danhSachPTGT.Find(x => x.ID == id);
            if (ptgt != null)
            {
                danhSachPTGT.Remove(ptgt);
                Console.WriteLine("Xóa phương tiện thành công.");
            }
            else
            {
                Console.WriteLine("Không tìm thấy phương tiện có ID {0}.", id);
            }
        }

        public void TimPhuongTien(string hangSX, string mauXe)
        {
            List<PhuongTienGiaoThong> phuongTienTimThay = danhSachPTGT.FindAll(x => x.HangSanXuat == hangSX && x.MauXe == mauXe);
            if (phuongTienTimThay.Count > 0)
            {
                Console.WriteLine("----- Các phương tiện tìm thấy -----");
                foreach (PhuongTienGiaoThong ptgt in phuongTienTimThay)
                {
                    Console.WriteLine("ID: {0}", ptgt.ID);
                    Console.WriteLine("Hãng sản xuất: {0}", ptgt.HangSanXuat);
                    Console.WriteLine("Năm sản xuất: {0}", ptgt.NamSanXuat);
                    Console.WriteLine("Giá bán: {0}", ptgt.GiaBan);
                    Console.WriteLine("Màu xe: {0}", ptgt.MauXe);
                    Console.WriteLine("------------------------------------");
                }
            }
            else
            {
                Console.WriteLine("Không tìm thấy phương tiện theo yêu cầu.");
            }
        }
    }
}
