﻿using System.Text;

namespace Bai6
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.UTF8;
            QuanLyHocSinh quanLyHocSinh = new QuanLyHocSinh();

            bool exit = false;
            while (!exit)
            {
                Console.WriteLine("====== MENU ======");
                Console.WriteLine("1. Thêm học sinh");
                Console.WriteLine("2. Hiện thị các học sinh 20 tuổi.");
                Console.WriteLine("3. Cho biết số lượng các học sinh có tuổi là 23 và quê ở DN.");
                Console.WriteLine("4. Hiển thị thông tin");
                Console.WriteLine("0. Thoát");
                Console.WriteLine("==================");

                Console.Write("Nhập lựa chọn của bạn: ");
                int? luaChon = Utility.Validate.GetValidNumberInput();

                switch (luaChon)
                {
                    case 1:
                        Console.WriteLine("Thêm học sinh");
                        Console.WriteLine("Nhập tên");
                        string hoTen = Console.ReadLine();
                        Console.WriteLine("Nhập tuổi");
                        int tuoi = Utility.Validate.GetValidNumberInput();
                        Console.WriteLine("Nhập quê quán");
                        string queQuan  = Console.ReadLine();
                        HocSinh hocSinh = new HocSinh()
                        {
                            HoTen = hoTen,
                            Tuoi = tuoi,
                            QueQuan = queQuan
                        };

                        quanLyHocSinh.ThemHocSinh(hocSinh);
                        Console.WriteLine("Thêm thành công \n ==================");
                        break;

                    case 2:
                        quanLyHocSinh.HienThiHocSinh20();
                        break;
                    case 3:
                        quanLyHocSinh.DemHocSinh();
                        break;
                    case 4:
                        quanLyHocSinh.HienThiHocSinh();
                        break;

                    case 0:
                        exit = true;
                        break;
                }
               


            }
        }
    }
}