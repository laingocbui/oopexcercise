﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bai5
{
    public class KhachSan
    {
        private List<Nguoi> danhSachKhachHang;
        public List<Phong> danhSachPhong;

        public KhachSan()
        {
            danhSachKhachHang = new List<Nguoi>();
            danhSachPhong = new List<Phong>();
        }

        public void ThemKhachHang(Nguoi khachHang)
        {
            danhSachKhachHang.Add(khachHang);
        }

        public void XoaKhachHang(int soCMND)
        {
            Nguoi khachHang = danhSachKhachHang.Find(kh => kh.SoCMND == soCMND);
            if (khachHang != null)
            {
                danhSachKhachHang.Remove(khachHang);
                Console.WriteLine("Đã xoá khách hàng có số CMND: " + soCMND);
            }
            else
            {
                Console.WriteLine("Không tìm thấy khách hàng có số CMND: " + soCMND);
            }
        }

        public decimal TinhTienThuePhong(int soCMND, int soNgayThue)
        {
            Nguoi khachHang = danhSachKhachHang.Find(kh => kh.SoCMND == soCMND);
            if (khachHang != null)
            {
                Phong phong = danhSachPhong.Find(p => p.LoaiPhong == khachHang.LoaiPhong);
                if (phong != null)
                {
                    decimal tienThuePhong = phong.GiaPhong * soNgayThue;
                    return tienThuePhong;
                }
                else
                {
                    Console.WriteLine("Không tìm thấy thông tin phòng thuê của khách hàng có số CMND: " + soCMND);
                }
            }
            else
            {
                Console.WriteLine("Không tìm thấy khách hàng có số CMND: " + soCMND);
            }

            return 0;
        }

        public void HienThiDanhSachKhachHang()
        {
            Console.WriteLine("Danh sách khách hàng:");
            if (danhSachKhachHang.Count !=0)
            {
                foreach (Nguoi khachHang in danhSachKhachHang)
                {
                    Console.WriteLine("Họ tên: " + khachHang.HoTen);
                    Console.WriteLine("Tuổi: " + khachHang.Tuoi);
                    Console.WriteLine("Số CMND: " + khachHang.SoCMND);
                    Console.WriteLine("Loại phòng: " + khachHang.LoaiPhong);
                    Console.WriteLine("==============================");
                }
            }else { Console.WriteLine("ế khách"); }
            
        }
    }
}
