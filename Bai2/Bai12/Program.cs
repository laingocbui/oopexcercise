﻿using System.Text;

namespace Bai12
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.UTF8;
            QLPTGT qlptgt = new QLPTGT();

            string choice;
            do
            {
                Console.WriteLine("----------- MENU -----------");
                Console.WriteLine("1. Thêm phương tiện");
                Console.WriteLine("2. Xóa phương tiện");
                Console.WriteLine("3. Tìm phương tiện theo hãng sản xuất và màu");
                Console.WriteLine("4. Thoát");
                Console.WriteLine("-----------------------------");
                Console.Write("Vui lòng chọn: ");
                choice = Console.ReadLine();

                switch (choice)
                {
                    case "1":
                        Console.WriteLine("----- Thêm phương tiện -----");
                        string loaiPT = string.Empty;
                        bool isValid = false;

                        do
                        {
                            Console.Write("Loại phương tiện (O: Ô tô, M: Xe máy, T: Xe tải): ");
                            loaiPT = Console.ReadLine().ToUpper();

                            if (loaiPT == "M" || loaiPT == "N" || loaiPT == "O")
                            {
                                isValid = true;
                            }
                            else
                            {
                                Console.WriteLine("Nhập sai! Vui lòng nhập lại.");
                            }
                        } while (!isValid);

                        PhuongTienGiaoThong ptgt;
                        switch (loaiPT)
                        {
                            case "O":
                                ptgt = new Oto();
                                Console.Write("Số chỗ ngồi: ");
                                ((Oto)ptgt).SoChoNgoi = Utility.Validate.GetValidNumberInput();;
                                Console.Write("Kiểu động cơ: ");
                                ((Oto)ptgt).KieuDongCo = Console.ReadLine();
                                break;
                            case "M":
                                ptgt = new XeMay();
                                Console.Write("Công xuất: ");
                                ((XeMay)ptgt).CongXuat = Utility.Validate.GetValidNumberInput();;
                                break;
                            case "T":
                                ptgt = new XeTai();
                                Console.Write("Trọng tải: ");
                                ((XeTai)ptgt).TrongTai = Utility.Validate.GetValidNumberInput();;
                                break;
                            default:
                                Console.WriteLine("Lựa chọn không hợp lệ.");
                                continue;
                        }

                        Console.Write("ID: ");
                        ptgt.ID = Utility.Validate.GetValidNumberInput();
                        Console.Write("Hãng sản xuất: ");
                        ptgt.HangSanXuat = Console.ReadLine();
                       
                        string namSanXuat;
                        while (true)
                        {
                            Console.Write("Nam sản xuất (dd/mm/yyyy): ");
                            namSanXuat = Console.ReadLine();
                            
                                if (Utility.Validate.IsValidBirthDate(namSanXuat))
                                {
                                    break;
                                }
                                else
                                {
                                Console.WriteLine($"{namSanXuat} wrong format"); 
                                }
                            
                        }

                        
                        Console.Write("Giá bán: ");
                        ptgt.GiaBan = Utility.Validate.GetValidNumberInput();
                        Console.Write("Màu xe: ");
                        ptgt.MauXe = Console.ReadLine();

                        qlptgt.ThemPhuongTien(ptgt);
                        break;
                    case "2":
                        Console.WriteLine("----- Xóa phương tiện -----");
                        Console.Write("Nhập ID phương tiện cần xóa: ");
                        int id = Utility.Validate.GetValidNumberInput();
                        qlptgt.XoaPhuongTien(id);
                        break;
                    case "3":
                        Console.WriteLine("----- Tìm phương tiện -----");
                        Console.Write("Hãng sản xuất: ");
                        string hangSX = Console.ReadLine();
                        Console.Write("Màu xe: ");
                        string mauXe = Console.ReadLine();
                        qlptgt.TimPhuongTien(hangSX, mauXe);
                        break;
                    case "4":
                        Console.WriteLine("Đã thoát chương trình.");
                        break;
                    default:
                        Console.WriteLine("Lựa chọn không hợp lệ. Vui lòng chọn lại.");
                        break;
                }

                Console.WriteLine();
            } while (choice != "4");
        }
    }
    
}