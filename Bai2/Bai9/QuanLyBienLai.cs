﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bai9
{
    internal class QuanLyBienLai
    {
        private List<BienLai> bienLaiList;
        HashSet<string> idCheck = new HashSet<string>(); 

        public QuanLyBienLai()
        {
            bienLaiList = new List<BienLai>();
        }

        public void ThemBienLai()
        {
            Console.WriteLine("Nhap thong tin bien lai:");

            Console.Write("Ho ten chu ho: ");
            string hoTen = Console.ReadLine();

            Console.Write("So nha: ");
            string soNha = Console.ReadLine();

            Console.Write("Ma so cong to: ");
            string maSoCongTo = Console.ReadLine();

            Console.Write("Chi so cu: ");
            int chiSoCu;
            while (!int.TryParse(Console.ReadLine(), out chiSoCu))
            {
                Console.Write("Chi so cu: ");
            }

            Console.Write("Chi so moi: ");
            int chiSoMoi;
            while (!int.TryParse(Console.ReadLine(), out chiSoMoi))
            {
                Console.Write("Chi so moi: ");
            }

            KhachHang khachHang = new KhachHang
            {
                HoTen = hoTen,
                SoNha = soNha,
                MaSoCongTo = maSoCongTo
            };

            BienLai bienLai = new BienLai
            {
                KhachHang = khachHang,
                ChiSoCu = chiSoCu,
                ChiSoMoi = chiSoMoi
            };

            if (idCheck.Contains(khachHang.MaSoCongTo))
            {
                Console.WriteLine("Mã số công tơ đã tồn tại");
                return;
            }
            idCheck.Add(khachHang.MaSoCongTo);
            bienLaiList.Add(bienLai);

            Console.WriteLine("\nDa them bien lai thanh cong.");
        }

        public void SuaBienLai()
        {
            if (bienLaiList.Count == 0)
            {
                Console.WriteLine("Danh sach bien lai rong.");
                return;
            }

            Console.Write("Nhap ma so cong to: ");
            string maSoCongTo = Console.ReadLine();

            BienLai bienLai = bienLaiList.Find(b => b.KhachHang.MaSoCongTo == maSoCongTo);

            if (bienLai == null)
            {
                Console.WriteLine("Khong tim thay bien lai voi ma so cong to da nhap.");
                return;
            }

            Console.WriteLine("Thong tin bien lai can sua:");
            Console.WriteLine("Ho ten chu ho: " + bienLai.KhachHang.HoTen);
            Console.WriteLine("So nha: " + bienLai.KhachHang.SoNha);
            Console.WriteLine("Ma so cong to: " + bienLai.KhachHang.MaSoCongTo);
            Console.WriteLine("Chi so cu: " + bienLai.ChiSoCu);
            Console.WriteLine("Chi so moi: " + bienLai.ChiSoMoi);

            Console.WriteLine("\nNhap thong tin moi:");

            Console.Write("Ho ten chu ho: ");
            string hoTen = Console.ReadLine();

            Console.Write("So nha: ");
            string soNha = Console.ReadLine();

            Console.Write("Chi so cu: ");
            int chiSoCu;
            while (!int.TryParse(Console.ReadLine(), out chiSoCu))
            {
                Console.Write("Chi so cu: ");
            }

            Console.Write("Chi so moi: ");
            int chiSoMoi;
            while (!int.TryParse(Console.ReadLine(), out chiSoMoi))
            {
                Console.Write("Chi so moi: ");
            }

            bienLai.KhachHang.HoTen = hoTen;
            bienLai.KhachHang.SoNha = soNha;
            bienLai.ChiSoCu = chiSoCu;
            bienLai.ChiSoMoi = chiSoMoi;

            Console.WriteLine("\nDa sua bien lai thanh cong.");
        }

        public void XoaBienLai()
        {
            if (bienLaiList.Count == 0)
            {
                Console.WriteLine("Danh sach bien lai rong.");
                return;
            }

            Console.Write("Nhap ma so cong to: ");
            string maSoCongTo = Console.ReadLine();

            BienLai bienLai = bienLaiList.Find(b => b.KhachHang.MaSoCongTo == maSoCongTo);

            if (bienLai == null)
            {
                Console.WriteLine("Khong tim thay bien lai voi ma so cong to da nhap.");
                return;
            }

            bienLaiList.Remove(bienLai);

            Console.WriteLine("\nDa xoa bien lai thanh cong.");
        }

        public void TinhTienDien()
        {
            if (bienLaiList.Count == 0)
            {
                Console.WriteLine("Danh sach bien lai rong.");
                return;
            }

            Console.Write("Nhap ma so cong to: ");
            string maSoCongTo = Console.ReadLine();

            BienLai bienLai = bienLaiList.Find(b => b.KhachHang.MaSoCongTo == maSoCongTo);

            if (bienLai == null)
            {
                Console.WriteLine("Khong tim thay bien lai voi ma so cong to da nhap.");
                return;
            }

            int tienDien = bienLai.TinhTienDien();

            Console.WriteLine("Tien dien can thanh toan: " + tienDien);
        }
    }
}
