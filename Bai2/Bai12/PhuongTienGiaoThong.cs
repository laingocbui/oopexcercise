﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bai12
{
    internal class PhuongTienGiaoThong
    {
        public int ID { get; set; }
        public string HangSanXuat { get; set; }
        public string NamSanXuat { get; set; }
        public int GiaBan { get; set; }
        public string MauXe { get; set; }
    }
}
