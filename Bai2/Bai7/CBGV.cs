﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bai7
{
    internal class CBGV : Nguoi
    {
        private List<Nguoi> nguoiList;
        public decimal LuongCung { get; set; }
        public decimal LuongThuong { get; set; }
        public decimal TienPhat { get; set; }
        public decimal LuongThucLinh { get; private set; }
       // public Nguoi Nguoi { get; set; }
        public CBGV()
        {
            nguoiList = new List<Nguoi>();
        }

        public void TinhLuongThucLinh()
        {
            LuongThucLinh = LuongCung + LuongThuong - TienPhat;
        }

        public void ThemCBGV(Nguoi nguoi)
        {
            nguoiList.Add(nguoi);
            Console.WriteLine("theem thanh cong");
        }

        public void XoaCBGV(string manv)
        {
            Nguoi timNguoi = nguoiList.Find(x=>x.MaNV ==  manv);
            if (timNguoi == null)
            {
                Console.WriteLine("ko tìm thấy nhân viên");
                return;
            }
            nguoiList.Remove(timNguoi);
            Console.WriteLine("xóa thành công");
        }
    }
}
