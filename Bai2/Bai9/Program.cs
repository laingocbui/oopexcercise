﻿namespace Bai9
{
    internal class Program
    {
        static void Main(string[] args)
        {
            QuanLyBienLai quanLyBienLai = new QuanLyBienLai();

            Console.WriteLine("==== Quan Ly Bien Lai Thu Tien Dien ====");

            while (true)
            {
                Console.WriteLine("\nLua chon:");
                Console.WriteLine("1. Them bien lai");
                Console.WriteLine("2. Sua bien lai");
                Console.WriteLine("3. Xoa bien lai");
                Console.WriteLine("4. Tinh tien dien");
                Console.WriteLine("5. Thoat");

                Console.Write("Nhap lua chon cua ban: ");
                string luaChon = Console.ReadLine();
                //while (!int.TryParse(Console.ReadLine(), out luaChon))
                //{
                //    Console.Write("Nhap lua chon cua ban: ");
                //}

                Console.WriteLine();

                switch (luaChon)
                {
                    case "1":
                        quanLyBienLai.ThemBienLai();
                        break;
                    case "2":
                        quanLyBienLai.SuaBienLai();
                        break;
                    case "3":
                        quanLyBienLai.XoaBienLai();
                        break;
                    case "4":
                        quanLyBienLai.TinhTienDien();
                        break;
                    case "5":
                        Console.WriteLine("Cam on ban da su dung chuong trinh.");
                        return;
                    default:
                        Console.WriteLine("Lua chon khong hop le.");
                        break;
                }
            }
        }
    }
}