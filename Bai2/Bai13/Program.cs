﻿using static Bai13.Exceptions;
using Utility;

namespace Bai13
{
    internal class Program
    {
        static void Main(string[] args)
        {
            EmployeeManager employeeManager = new EmployeeManager();
            
            bool exit = false;

            while (!exit)
            {
                Console.WriteLine("EMPLOYEE MANAGEMENT SYSTEM");
                Console.WriteLine("1. Add Employee");
                Console.WriteLine("2. Remove Employee");
                Console.WriteLine("3. Find Employees by Type");
                Console.WriteLine("4. Display All Employees");
                Console.WriteLine("5. Exit");

                Console.Write("Enter your choice: ");
                string choice = Console.ReadLine();

                Console.WriteLine();

                switch (choice)
                {
                    case "1":
                        try
                        {
                            Console.WriteLine("Add Employee");

                            Console.Write("Employee Type (0: Experience, 1: Fresher, 2: Intern): ");
                            int employeeType = Utility.Validate.GetValidNumberInput();

                            Console.Write("ID: ");
                            string id = Console.ReadLine();

                            Console.Write("Full Name: ");
                            string fullName = Console.ReadLine();

                            string birthDay;
                            while (true)
                            {
                                Console.Write("Birth Day (dd/mm/yyyy): ");
                                birthDay = Console.ReadLine();
                                try
                                {
                                    if (Utility.Validate.IsValidBirthDate(birthDay))
                                    {
                                        break;
                                    }
                                    else
                                    {
                                        throw new BirthDayException($"{birthDay} wrong format");
                                    }
                                }
                                catch (BirthDayException ex)
                                {
                                    Console.WriteLine(ex.Message);
                                }
                            }

                            string phone;
                            while (true)
                            {
                                Console.Write("Phone: ");
                                phone = Console.ReadLine();
                                try
                                {
                                    if (Utility.Validate.IsValidPhoneNumber(phone))
                                    {
                                        break;
                                    }
                                    else
                                    {
                                        throw new PhoneException($"{phone} wrong format");
                                    }
                                }
                                catch (PhoneException ex)
                                {
                                    Console.WriteLine(ex.Message);
                                }
                            }
                            

                            
                            string email;
                            while (true)
                            {
                                Console.Write("Email: ");
                                email = Console.ReadLine();
                                try
                                {
                                    if (Utility.Validate.IsValidEmail(email))
                                    {
                                        break;
                                    }
                                    else
                                    {
                                        throw new  EmailException($"{email} wrong format");
                                    }
                                }
                                catch (EmailException ex)
                                {
                                    Console.WriteLine(ex.Message);
                                }
                            }
                            Console.WriteLine("Certificate Number:");
                            List<Certificate> certificateList = new List<Certificate>();
                            int certificateNumber = Utility.Validate.GetValidNumberInput();
                            for (int i = 0; i < certificateNumber; i++)
                            {
                                Console.WriteLine("Enter CertificateId: ");
                                int certificateId = Utility.Validate.GetValidNumberInput();
                                Console.WriteLine("Enter Certificate Name: ");
                                string certificateName = Console.ReadLine();
                                Console.WriteLine("Enter Certificate rank: ");
                                string certificateRank = Console.ReadLine();
                                
                                string certificateDate;
                                while (true)
                                {
                                    Console.Write("Certificate Date (dd/mm/yyyy): ");
                                    certificateDate = Console.ReadLine();
                                    try
                                    {
                                        if (Utility.Validate.IsValidBirthDate(certificateDate))
                                        {
                                            break;
                                        }
                                        else
                                        {
                                            throw new BirthDayException($"{certificateDate} wrong format");
                                        }
                                    }
                                    catch (BirthDayException ex)
                                    {
                                        Console.WriteLine(ex.Message);
                                    }
                                }
                                Certificate certificate = new Certificate(certificateId, certificateName, certificateRank, DateTime.Parse( certificateDate));
                                certificateList.Add(certificate);

                            }

                            if (employeeType == 0)
                            {
                                Console.Write("Years of Experience: ");
                                int expInYear = Utility.Validate.GetValidNumberInput();

                                Console.Write("Professional Skill: ");
                                string proSkill = Console.ReadLine();

                                Experience experience = new Experience(id, fullName, DateTime.Parse(birthDay), phone, email, certificateList, expInYear, proSkill );
                                employeeManager.AddEmployee(experience);
                            }
                            else if (employeeType == 1)
                            {
                                
                                string graduationDate;
                                while (true)
                                {
                                    Console.Write("Graduation Date (dd/mm/yyyy): ");
                                    graduationDate = Console.ReadLine();
                                    try
                                    {
                                        if (Utility.Validate.IsValidBirthDate(graduationDate))
                                        {
                                            break;
                                        }
                                        else
                                        {
                                            throw new BirthDayException($"{graduationDate} wrong format");
                                        }
                                    }
                                    catch (BirthDayException ex)
                                    {
                                        Console.WriteLine(ex.Message);
                                    }
                                }

                                Console.Write("Graduation Rank: ");
                                string graduationRank = Console.ReadLine();

                                Console.Write("Education: ");
                                string education = Console.ReadLine();

                                Fresher fresher = new Fresher(id, fullName, DateTime.Parse(birthDay), phone, email, certificateList, DateTime.Parse(graduationDate), graduationRank, education);
                                employeeManager.AddEmployee(fresher);
                            }
                            else if (employeeType == 2)
                            {
                                Console.Write("Majors: ");
                                string majors = Console.ReadLine();

                                Console.Write("Semester: ");
                                int semester = Utility.Validate.GetValidNumberInput();

                                Console.Write("University Name: ");
                                string universityName = Console.ReadLine();

                                Intern intern = new Intern(id, fullName, DateTime.Parse(birthDay), phone, email, certificateList, majors, semester, universityName);
                                employeeManager.AddEmployee(intern);
                            }

                            Console.WriteLine("Employee added successfully.");
                            Console.WriteLine();
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.Message);
                        }
                        break;


                    case "2":
                        Console.WriteLine("Remove Employee");

                        Console.Write("Enter Employee ID: ");
                        string removeId = Console.ReadLine();

                        employeeManager.RemoveEmployee(removeId);

                        Console.WriteLine("Employee removed successfully.");
                        Console.WriteLine();
                        break;

                    case "3":
                        Console.WriteLine("Find Employees by Type");

                        int findEmployeeType;
                        Console.Write("Enter Employee Type (0: Experience, 1: Fresher, 2: Intern): ");

                        while (true)
                        {
                            
                            string input = Console.ReadLine();

                            if (int.TryParse(input, out findEmployeeType))
                            {
                                break; 
                            }
                            else
                            {
                                Console.Write("Choose Correct Type ( 1 or 2 or 0): ");
                            }
                        }

                        var employeesByType = employeeManager.FindEmployeesByType(findEmployeeType);

                        //Console.WriteLine("Employees found:");

                        foreach (var employee in employeesByType)
                        {
                            employee.ShowInfo();
                            Console.WriteLine();
                        }

                        Console.WriteLine();
                        break;

                    case "4":
                        Console.WriteLine("Display All Employees");

                        employeeManager.DisplayAllEmployees();

                        Console.WriteLine();
                        break;

                    case "5":
                        exit = true;
                        break;

                    default:
                        Console.WriteLine("Invalid choice. Please try again.");
                        Console.WriteLine();
                        break;
                }
            }
        }
        
    }
}