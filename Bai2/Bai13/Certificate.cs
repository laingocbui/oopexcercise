﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bai13
{
    public class Certificate
    {
        public int CertificateID { get; set; }
        public string CertificateName { get; set; }
        public string CertificateRank { get; set; }
        public DateTime CertificateDate { get; set; }
    

        public Certificate(int certificateId, string certificateName, string certificateRank, DateTime certificateDate)
        {
            CertificateID = certificateId;
            CertificateName = certificateName;
            CertificateRank = certificateRank;
            CertificateDate = certificateDate;
        }
        public override string ToString()
        {
            return $"CertificateID: {CertificateID}\n" +
                   $"CertificateName: {CertificateName}\n" +
                   $"CertificateRank: {CertificateRank}\n" +
                   $"CertificateDate: {CertificateDate}\n";
        }

    }
}
