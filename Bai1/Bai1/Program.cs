﻿namespace Bai1
{
    internal class Program
    {
        static void Main(string[] args)
        {
            ManagerOfficer managerOfficer = new ManagerOfficer();
            while (true)
            {
                Console.WriteLine("Application Manager Officer");
                Console.WriteLine("Enter 1: To insert officer");
                Console.WriteLine("Enter 2: To search officer by name: ");
                Console.WriteLine("Enter 3: To show information officers");
                Console.WriteLine("Enter 4: To exit:");
                string line = Console.ReadLine();
                switch (line)
                {
                    case "1":
                        {
                            Console.WriteLine("Enter a: to insert Engineer");
                            Console.WriteLine("Enter b: to insert Worker");
                            Console.WriteLine("Enter c: to insert Staff");
                            string type = Console.ReadLine();
                            switch (type)
                            {
                                case "a":
                                    {
                                        Console.Write("Enter name: ");
                                        string name = Console.ReadLine();
                                        Console.Write("Enter age:");
                                        int age = Utility.Validate.GetValidNumberInput();
                                        Console.Write("Enter gender: ");
                                        string gender = Console.ReadLine();
                                        Console.Write("Enter address: ");
                                        string address = Console.ReadLine();
                                        Console.Write("Enter branch: ");
                                        string branch = Console.ReadLine();
                                        Officer engineer = new Engineer(name, age, gender, address, branch);
                                        managerOfficer.AddOfficer(engineer);
                                        Console.WriteLine(engineer.ToString());
                                        break;
                                    }

                                case "b":
                                    {
                                        Console.Write("Enter name: ");
                                        string name = Console.ReadLine();
                                        Console.Write("Enter age:");
                                        int age = Utility.Validate.GetValidNumberInput();
                                        Console.Write("Enter gender: ");
                                        string gender = Console.ReadLine();
                                        Console.Write("Enter address: ");
                                        string address = Console.ReadLine();
                                        Console.Write("Enter level: ");
                                        int level = Utility.Validate.GetValidNumberInput();
                                        Officer worker = new Worker(name, age, gender, address, level);
                                        managerOfficer.AddOfficer(worker);
                                        Console.WriteLine(worker.ToString());
                                        break;
                                    }

                                case "c":
                                    {
                                        Console.Write("Enter name: ");
                                        string name = Console.ReadLine();
                                        Console.Write("Enter age: ");
                                        int age = Utility.Validate.GetValidNumberInput();
                                        Console.Write("Enter gender: ");
                                        string gender = Console.ReadLine();
                                        Console.Write("Enter address: ");
                                        string address = Console.ReadLine();
                                        Console.Write("Enter task: ");
                                        string task = Console.ReadLine();
                                        Officer staff = new Employee(name, age, gender, address, task);
                                        managerOfficer.AddOfficer(staff);
                                        Console.WriteLine(staff.ToString());
                                        break;
                                    }
                                default:
                                    Console.WriteLine("Invalid");
                                    break;
                            }
                            break;
                        }

                    case "2":
                        {
                            Console.Write("Enter name to search: ");
                            string name = Console.ReadLine();
                            List<Officer> officers = managerOfficer.SearchOfficerByName(name);
                            foreach (Officer officer in officers)
                            {
                                Console.WriteLine(officer.ToString());
                            }
                            break;
                        }

                    case "3":
                        {
                            managerOfficer.ShowListInforOfficer();
                            break;
                        }

                    case "4":
                        {
                            return;
                        }

                    default:
                        Console.WriteLine("Invalid");
                        continue;
                }
            }
        }
    }
}