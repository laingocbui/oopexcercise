﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bai13
{
    internal class EmployeeManager
    {
        private List<Employee> employees;
        HashSet<string> employeeSet = new HashSet<string>();
        public EmployeeManager()
        {
            employees = new List<Employee>();
        }

        
        public void AddEmployee(Employee employee)
        {
            if (employeeSet.Contains(employee.ID))
            {
                Console.WriteLine("ID Already exist");
                return;
            }
            employeeSet.Add(employee.ID);
            employees.Add(employee);
        }

       
        public void RemoveEmployee(string id)
        {
            Employee employee = employees.Find(emp => emp.ID == id);
            if (employee != null)
            {
                employees.Remove(employee);
                employee.Employee_count--;
                Console.WriteLine("Employee removed successfully.");
            }
            else
            {
                Console.WriteLine("employee not found");
            }
            
            
        }

   
        public List<Employee> FindEmployeesByType(int employeeType)
        {
            var query =  employees.FindAll(emp => emp.Employee_type == employeeType);
            if (query.Count == 0)
            {
                Console.WriteLine("Have No Employee");
            }
            return query;
        }

    
        public void DisplayAllEmployees()
        {
            if (employees.Count !=0)
            {
                foreach (Employee employee in employees)
                {
                    employee.ShowInfo();
                    Console.WriteLine($"All exist employee : {employee.Employee_count}");
                }
            }
            else
            {
                Console.WriteLine("Have No Employee!");
            }
            
        }
    }
}
