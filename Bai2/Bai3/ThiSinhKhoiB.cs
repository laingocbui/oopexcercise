﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bai3
{
    public class ThiSinhKhoiB : ThiSinh
    {
        public List<string> MonThi { get; set; }

        public ThiSinhKhoiB()
        {
            MonThi = new List<string> { "Toán", "Hóa", "Sinh" };
        }
    }
}
