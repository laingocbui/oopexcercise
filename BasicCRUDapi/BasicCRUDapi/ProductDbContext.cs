﻿using Microsoft.EntityFrameworkCore;

namespace BasicCRUDapi
{
    public class ProductDbContext : DbContext
    {
        public ProductDbContext(DbContextOptions<ProductDbContext> options) : base(options)
        {
        }


        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            string con = "Server=.;Database=BasicCRUD;Trusted_Connection=True;TrustServerCertificate=True;MultipleActiveResultSets=True;";
            optionsBuilder.UseSqlServer(con);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Product>().HasData(new Product()
            {
                Id = 1,
                Name = "san phẩm 1",
                Description = "mo ta1 "
                
            },
              new Product
              {
                  Id = 2,
                  Name = "san phẩm 2",
                  Description = "mo ta1 "

              },
              new Product
              {
                  Id = 3,
                  Name = "san phẩm 3",
                  Description = "mo ta1 "

              },
              new Product
              {
                  Id = 4,
                  Name = "san phẩm 4",
                  Description = "mo ta1 "

              },
              new Product
              {
                  Id = 5,
                  Name = "san phẩm 5",
                  Description = "mo ta1 "
              });


        }
        public DbSet<Product> Products { get; set; }

    }
}
