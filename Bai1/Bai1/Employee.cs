﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Bai1
{
    public class Employee : Officer
    {
        public string Task { get; set; }

        public Employee(string name, int age, string gender, string address, string task)
            : base(name, age, gender, address)
        {
            Task = task;
        }

        public override string ToString()
        {
            return $"Employee{{branch='{Task}', name='{Name}', age={Age}, gender='{Gender}', address='{Address}'}}";

        }
    }
}
