﻿using System.Text;

namespace Bai3
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.UTF8;
            TuyenSinh tuyenSinh = new TuyenSinh();

            while (true)
            {
                Console.WriteLine("===== Quản lý thông tin thí sinh =====");
                Console.WriteLine("1. Thêm mới thí sinh");
                Console.WriteLine("2. Hiển thị thông tin thí sinh");
                Console.WriteLine("3. Tìm kiếm theo số báo danh");
                Console.WriteLine("4. Thoát khỏi chương trình");
                Console.WriteLine("======================================");
                Console.Write("Vui lòng chọn chức năng (1-4): ");
                string choice = Console.ReadLine();

                switch (choice)
                {
                    case "1":
                        Console.WriteLine("===== Thêm mới thí sinh =====");
                        Console.Write("Nhập số báo danh: ");
                        string soBaoDanh = Console.ReadLine();
                        Console.Write("Nhập họ tên: ");
                        string hoTen = Console.ReadLine();
                        Console.Write("Nhập địa chỉ: ");
                        string diaChi = Console.ReadLine();
                        Console.Write("Nhập mức ưu tiên: ");
                        int mucUuTien = Utility.Validate.GetValidNumberInput();
                        

                        ThiSinh thiSinh ;
                        string khoiThi = string.Empty;
                        while (true)
                        {
                            Console.Write("Nhập khối thi (A, B, C): ");
                            khoiThi = Console.ReadLine();

                            if (khoiThi.ToUpper() == "A")
                            {
                                thiSinh = new ThiSinhKhoiA();
                                break;
                            }
                            else if (khoiThi.ToUpper() == "B")
                            {
                                thiSinh = new ThiSinhKhoiB();
                                break;
                            }
                            else if (khoiThi.ToUpper() == "C")
                            {
                                thiSinh = new ThiSinhKhoiC();
                                break;
                            }
                            else
                            {
                                Console.WriteLine("Khối thi không hợp lệ! Vui lòng nhập lại.");
                            }
                        }

                        thiSinh.SoBaoDanh = soBaoDanh;
                        thiSinh.HoTen = hoTen;
                        thiSinh.DiaChi = diaChi;
                        thiSinh.MucUuTien = mucUuTien;

                        tuyenSinh.ThemMoiThiSinh(thiSinh);
                        break;

                    case "2":
                        Console.WriteLine("===== Hiển thị thông tin thí sinh =====");
                        tuyenSinh.HienThiThongTin();
                        break;

                    case "3":
                        Console.WriteLine("===== Tìm kiếm theo số báo danh =====");
                        Console.Write("Nhập số báo danh: ");
                        string sbd = Console.ReadLine();
                        tuyenSinh.TimKiemTheoSoBaoDanh(sbd);
                        break;

                    case "4":
                        Console.WriteLine("Thoát khỏi chương trình...");
                        Environment.Exit(0);
                        break;

                    default:
                        Console.WriteLine("Vui lòng chọn chức năng từ 1 đến 4.");
                        break;
                }

                Console.WriteLine();
            }
        }
    }
}