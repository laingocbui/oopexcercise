﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Reflection.Emit;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Bai1
{
    public class Worker : Officer
    {
        public int Level { get; set; }

        public Worker(string name, int age, string gender, string address, int level)
        : base(name, age, gender, address)
        {
            Level = level;
        }

        public override string ToString()
        {
            return $"Worker{{branch='{Level}', name='{Name}', age={Age}, gender='{Gender}', address='{Address}'}}";

        }

    }
}
