﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bai8
{
    internal class TheMuon
    {
        public string MaPhieuMuon { get; set; }
        public string NgayMuon { get; set; }
        public int HanTra { get; set; }
        public string SoHieuSach { get; set; }
        public SinhVien SinhVienMuon { get; set; }
    }
}
