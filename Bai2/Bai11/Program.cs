﻿namespace Bai11
{
    internal class Program
    {
        static void Main(string[] args)
        {
            SoPhuc soPhuc1 = new SoPhuc(0, 0);
            SoPhuc soPhuc2 = new SoPhuc(0, 0);

            Console.WriteLine("Nhập số phức thứ nhất:");
            soPhuc1.NhapSoPhuc();

            Console.WriteLine("Nhập số phức thứ hai:");
            soPhuc2.NhapSoPhuc();

            Console.WriteLine("Số phức thứ nhất:");
            soPhuc1.HienThiSoPhuc();

            Console.WriteLine("Số phức thứ hai:");
            soPhuc2.HienThiSoPhuc();

            SoPhuc tong = soPhuc1.CongSoPhuc(soPhuc2);
            Console.WriteLine("Tổng 2 số phức:");
            tong.HienThiSoPhuc();

            SoPhuc tich = soPhuc1.NhanSoPhuc(soPhuc2);
            Console.WriteLine("Tích 2 số phức:");
            tich.HienThiSoPhuc();

            Console.ReadLine();
        }
    }
}