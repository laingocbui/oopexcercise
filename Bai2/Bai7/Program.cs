﻿namespace Bai7
{
    internal class Program
    {
        static void Main(string[] args)
        {
            CBGV canBo = new CBGV() { };

            List<Nguoi> nguoi = new List<Nguoi>()
            {
                new Nguoi(){HoTen = "GV1", QueQuan = "HN", Tuoi = 20, MaNV = "NV01"},
                new Nguoi(){HoTen = "GV2", QueQuan = "HN1", Tuoi = 21, MaNV = "NV02"},
                new Nguoi(){HoTen = "GV3", QueQuan = "HN2", Tuoi = 22, MaNV = "NV03"},
                
            };
            Console.WriteLine("ten:");
            string hoten = Console.ReadLine();
            Console.WriteLine("que:");
            string queQuan = Console.ReadLine();
            Console.WriteLine("tuoi:");
            int tuoi = Utility.Validate.GetValidNumberInput();
            Console.WriteLine("manv:");
            string manv = Console.ReadLine();
            Nguoi nguoi1 = new Nguoi()
            {
                HoTen = hoten,
                Tuoi = tuoi,
                QueQuan = queQuan,
                MaNV = manv
            };

            canBo.ThemCBGV(nguoi1);
            canBo.XoaCBGV("NV01");
        }
    }
}