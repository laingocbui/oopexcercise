﻿using System.Text;

namespace Bai10
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.UTF8;
            VanBan vanBan = new VanBan();

            Console.WriteLine("===== XU LY VAN BAN =====");

            while (true)
            {
                Console.WriteLine("\nLua chon:");
                Console.WriteLine("1. Nhap van ban");
                Console.WriteLine("2. Dem so tu");
                Console.WriteLine("3. Dem so luong ky tu A");
                Console.WriteLine("4. Chuan hoa van ban");
                Console.WriteLine("5. Thoat");

                Console.Write("Nhap lua chon cua ban: ");
                string luaChon = Console.ReadLine();
                //while (!int.TryParse(Console.ReadLine(), out luaChon))
                //{
                //    Console.Write("Nhap lua chon cua ban: ");
                //}

                Console.WriteLine();
                string st = string.Empty;
                switch (luaChon)
                {
                    case "1":
                        while (true)
                        {
                            Console.Write("Nhập văn bản: ");
                            st = Console.ReadLine();

                            if (st == "")
                            {
                                Console.WriteLine("Văn bản bạn nhập không có ký tự nào!");
                                Console.Write("Bạn muốn nhập lại không (Y/N): ");
                                string choice = Console.ReadLine();

                                if (choice.ToUpper() == "Y")
                                {
                                    continue;
                                }
                                else
                                {
                                    break;
                                }
                            }

                           
                            vanBan.Text = st;
                            Console.WriteLine("Đã nhập văn bản.");

                            break;
                        }
                        break;

                    case "2":
                        if (KiemTraVanBan(vanBan.Text))
                        {
                            Console.WriteLine("So tu trong van ban: " + vanBan.DemSoTu());

                        }

                        break;

                    case "3":
                        if (KiemTraVanBan(vanBan.Text))
                        {
                            Console.WriteLine("So luong ky tu A trong van ban: " + vanBan.DemSoLuongKyTuA());

                        }
                        break;
                    case "4":
                        if (KiemTraVanBan(vanBan.Text))
                        {
                            vanBan.ChuanHoaVanBan();
                            Console.WriteLine("Van ban sau khi chuan hoa: " + vanBan.Text);

                        }
                        break;
                    case "5":
                        Console.WriteLine("Cam on ban da su dung chuong trinh.");
                        return;
                    default:
                        Console.WriteLine("Lua chon khong hop le.");
                        break;
                }
            }
        }

        static bool KiemTraVanBan(string vanban)
        {
            if (string.IsNullOrEmpty(vanban))
            {
                Console.WriteLine("Văn bản rỗng");
                return false;
            }
            else
            {
                return true;
            }
        }
    }
}