﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bai3
{
    public class TuyenSinh
    {
        private List<ThiSinh> danhSachThiSinh;

        public TuyenSinh()
        {
            danhSachThiSinh = new List<ThiSinh>();
        }

        public void ThemMoiThiSinh(ThiSinh thiSinh)
        {
            danhSachThiSinh.Add(thiSinh);
            Console.WriteLine("Thêm mới thành công!");
        }

        public void HienThiThongTin()
        {
            if (danhSachThiSinh.Count != 0)
            {
                foreach (var thiSinh in danhSachThiSinh)
                {
                    Console.WriteLine("Số báo danh: " + thiSinh.SoBaoDanh);
                    Console.WriteLine("Họ tên: " + thiSinh.HoTen);
                    Console.WriteLine("Địa chỉ: " + thiSinh.DiaChi);
                    Console.WriteLine("Mức ưu tiên: " + thiSinh.MucUuTien);

                    if (thiSinh is ThiSinhKhoiA)
                    {
                        Console.WriteLine("Khối thi: A");
                        Console.WriteLine("Môn thi: " + string.Join(", ", ((ThiSinhKhoiA)thiSinh).MonThi));
                    }
                    else if (thiSinh is ThiSinhKhoiB)
                    {
                        Console.WriteLine("Khối thi: B");
                        Console.WriteLine("Môn thi: " + string.Join(", ", ((ThiSinhKhoiB)thiSinh).MonThi));
                    }
                    else if (thiSinh is ThiSinhKhoiC)
                    {
                        Console.WriteLine("Khối thi: C");
                        Console.WriteLine("Môn thi: " + string.Join(", ", ((ThiSinhKhoiC)thiSinh).MonThi));
                    }

                    Console.WriteLine("===================================");
                }
            }
            else { Console.WriteLine("không có thí sinh nào!"); }
            
        }

        public void TimKiemTheoSoBaoDanh(string soBaoDanh)
        {
            ThiSinh thiSinh = danhSachThiSinh.Find(ts => ts.SoBaoDanh == soBaoDanh);
            if (thiSinh != null)
            {
                Console.WriteLine("Số báo danh: " + thiSinh.SoBaoDanh);
                Console.WriteLine("Họ tên: " + thiSinh.HoTen);
                Console.WriteLine("Địa chỉ: " + thiSinh.DiaChi);
                Console.WriteLine("Mức ưu tiên: " + thiSinh.MucUuTien);

                if (thiSinh is ThiSinhKhoiA)
                {
                    Console.WriteLine("Khối thi: A");
                    Console.WriteLine("Môn thi: " + string.Join(", ", ((ThiSinhKhoiA)thiSinh).MonThi));
                }
                else if (thiSinh is ThiSinhKhoiB)
                {
                    Console.WriteLine("Khối thi: B");
                    Console.WriteLine("Môn thi: " + string.Join(", ", ((ThiSinhKhoiB)thiSinh).MonThi));
                }
                else if (thiSinh is ThiSinhKhoiC)
                {
                    Console.WriteLine("Khối thi: C");
                    Console.WriteLine("Môn thi: " + string.Join(", ", ((ThiSinhKhoiC)thiSinh).MonThi));
                }
            }
            else
            {
                Console.WriteLine("Không tìm thấy thí sinh với số báo danh đã nhập.");
            }
        }
    }
}
