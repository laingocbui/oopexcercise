﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bai11
{
    internal class SoPhuc
    {
        public double PhanThuc { get; set; }
        public double PhanAo { get; set; }

        public SoPhuc()
        {
            PhanThuc = 0;
            PhanAo = 0;
        }

        public SoPhuc(double phanThuc, double phanAo)
        {
            PhanThuc = phanThuc;
            PhanAo = phanAo;
        }

        public void NhapSoPhuc()
        {
            Console.Write("Nhập phần thực: ");
            double phanThuc;
            double phanAo;
            while (!double.TryParse(Console.ReadLine(), out  phanThuc))
            {
                Console.WriteLine("Giá trị không hợp lệ! Vui lòng nhập lại phần thực: ");
            }
            PhanThuc = phanThuc;

            Console.Write("Nhập phần ảo: ");
            while (!double.TryParse(Console.ReadLine(), out phanAo))
            {
                Console.WriteLine("Giá trị không hợp lệ! Vui lòng nhập lại phần ảo: ");
            }
            PhanAo = phanAo;
        }

        
        public void HienThiSoPhuc()
        {
            Console.WriteLine($"Số phức: {PhanThuc} + {PhanAo}i");
        }

       
        public SoPhuc CongSoPhuc(SoPhuc soPhuc)
        {
            double tongPhanThuc = PhanThuc + soPhuc.PhanThuc;
            double tongPhanAo = PhanAo + soPhuc.PhanAo;
            return new SoPhuc(tongPhanThuc, tongPhanAo);
        }

        public SoPhuc NhanSoPhuc(SoPhuc soPhuc)
        {
            double tichPhanThuc = PhanThuc * soPhuc.PhanThuc - PhanAo * soPhuc.PhanAo;
            double tichPhanAo = PhanThuc * soPhuc.PhanAo + PhanAo * soPhuc.PhanThuc;
            return new SoPhuc(tichPhanThuc, tichPhanAo);
        }
    }
}
