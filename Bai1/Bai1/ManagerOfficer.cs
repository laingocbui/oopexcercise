﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bai1
{
    public class ManagerOfficer
    {
        private List<Officer> officers;

        public ManagerOfficer()
        {
            this.officers = new List<Officer>();
        }

        public void AddOfficer(Officer officer)
        {
            this.officers.Add(officer);
        }

        public List<Officer> SearchOfficerByName(string name)
        {
            return this.officers.Where(o => o.Name.Contains(name)).ToList();
        }

        public void ShowListInforOfficer()
        {
            this.officers.ForEach(o => Console.WriteLine(o.ToString()));
        }
    }
}
