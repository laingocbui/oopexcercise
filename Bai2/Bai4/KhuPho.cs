﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bai4
{
    public class KhuPho
    {
        public List<HoGiaDinh> HoGiaDinhList { get; set; }
        public KhuPho()
        {
            HoGiaDinhList = new List<HoGiaDinh>();
        }
        public void NhapHoGiaDinh()
        {
            Console.Write("Nhập số hộ dân trong khu phố: ");
            int n = Utility.Validate.GetValidNumberInput();

            for (int i = 0; i < n; i++)
            {
                Console.WriteLine($"\nNhập thông tin cho hộ gia đình thứ {i + 1}:");
                HoGiaDinh hoGiaDinh = new HoGiaDinh();

                Console.Write("Nhập số thành viên trong gia đình: ");
                hoGiaDinh.SoThanhVien = Utility.Validate.GetValidNumberInput();

                Console.Write("Nhập số nhà: ");
                hoGiaDinh.SoNha = Utility.Validate.GetValidNumberInput();

                for (int j = 0; j < hoGiaDinh.SoThanhVien; j++)
                {
                    Console.WriteLine($"\nNhập thông tin cho thành viên thứ {j + 1}:");
                    Nguoi nguoi = new Nguoi();

                    Console.Write("Nhập họ tên: ");
                    nguoi.HoTen = Console.ReadLine();

                    Console.Write("Nhập tuổi: ");
                    nguoi.Tuoi = Utility.Validate.GetValidNumberInput();

                    Console.Write("Nhập nghề nghiệp: ");
                    nguoi.NgheNghiep = Console.ReadLine();

                    Console.Write("Nhập số CMND: ");
                    nguoi.CMND = Utility.Validate.GetValidNumberInput();
                    while (!KiemTraSoCMNDDuyNhat(nguoi.CMND, hoGiaDinh.NguoiList))
                    {
                        Console.WriteLine("Số CMND đã tồn tại. Vui lòng nhập lại.");
                        Console.Write("Nhập số CMND: ");
                        nguoi.CMND = Utility.Validate.GetValidNumberInput();
                    }

                   
                    hoGiaDinh.NguoiList.Add(nguoi);
                }

                HoGiaDinhList.Add(hoGiaDinh);

               
            }
        }

        private bool KiemTraSoCMNDDuyNhat(int soCMND, List<Nguoi> danhSachNguoi)
        {
            foreach (Nguoi nguoi in danhSachNguoi)
            {
                if (nguoi.CMND == soCMND)
                {
                    return false; 
                }
            }

            return true; 
        }
        public void HienThiThongTinHoGiaDinh()
        {
            Console.WriteLine("\nDanh sách các hộ gia đình trong khu phố:");
            foreach (HoGiaDinh hoGiaDinh in HoGiaDinhList)
            {
                Console.WriteLine($"Số nhà: {hoGiaDinh.SoNha}");
                Console.WriteLine($"Số thành viên: {hoGiaDinh.SoThanhVien}");
                Console.WriteLine("Danh sách thành viên trong gia đình:");

                foreach (Nguoi nguoi in hoGiaDinh.NguoiList)
                {
                    Console.WriteLine($"Họ tên: {nguoi.HoTen}");
                    Console.WriteLine($"Tuổi: {nguoi.Tuoi}");
                    Console.WriteLine($"Nghề nghiệp: {nguoi.NgheNghiep}");
                    Console.WriteLine($"Số CMND: {nguoi.CMND}");
                    Console.WriteLine();
                }

                Console.WriteLine();
            }
        }
    } 
}
