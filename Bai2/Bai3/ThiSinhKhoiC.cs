﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bai3
{
    public class ThiSinhKhoiC : ThiSinh
    {
        public List<string> MonThi { get; set; }

        public ThiSinhKhoiC()
        {
            MonThi = new List<string> { "Văn", "Sử", "Địa" };
        }
    }
}
