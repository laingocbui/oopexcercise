﻿using System.Text;

namespace Bai5
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.UTF8;
            KhachSan khachSan = new KhachSan();
            HashSet<int> cmndSet = new HashSet<int>();

            khachSan.danhSachPhong.Add(new Phong { LoaiPhong = "A", GiaPhong = 500 });
            khachSan.danhSachPhong.Add(new Phong { LoaiPhong = "B", GiaPhong = 300 });
            khachSan.danhSachPhong.Add(new Phong { LoaiPhong = "C", GiaPhong = 100 });

            bool ketThuc = false;
            while (!ketThuc)
            {
                Console.WriteLine("====== MENU ======");
                Console.WriteLine("1. Thêm khách hàng");
                Console.WriteLine("2. Xoá khách hàng");
                Console.WriteLine("3. Tính tiền thuê phòng");
                Console.WriteLine("4. Hiển thị danh sách khách hàng");
                Console.WriteLine("0. Thoát");
                Console.WriteLine("==================");

                Console.Write("Nhập lựa chọn của bạn: ");
                string luaChon = Console.ReadLine();

                switch (luaChon)
                {
                    case "1":
                        Console.WriteLine("=== Thêm khách hàng ===");
                        Console.Write("Họ tên: ");
                        string hoTen = Console.ReadLine();
                        Console.Write("Tuổi: ");
                        int tuoi = Utility.Validate.GetValidNumberInput();
                        int soCMND;
                        while (true)
                        {
                            Console.Write("Số CMND: ");
                            soCMND = Utility.Validate.GetValidNumberInput();
                            if (cmndSet.Contains(soCMND))
                            {
                                Console.WriteLine("CMND Already exist");
                                
                            }
                            else
                            {
                                cmndSet.Add(soCMND);
                                break;
                            }
                            
                        }
                        

                        Nguoi khachHang = new Nguoi { HoTen = hoTen, Tuoi = tuoi, SoCMND = soCMND };
                        string loaiPhong;

                        while (true)
                        {
                            Console.WriteLine("Chọn loại phòng (A, B, C): ");
                            loaiPhong = Console.ReadLine().ToUpper();

                            if (loaiPhong == "A" || loaiPhong == "B" || loaiPhong == "C")
                            {
                                break;
                            }
                            else
                            {
                                Console.WriteLine("Loại phòng không hợp lệ! Vui lòng chọn lại.");
                            }
                        }

                        khachHang.LoaiPhong = loaiPhong;

                        khachSan.ThemKhachHang(khachHang);
                        Console.WriteLine("Thêm khách hàng thành công.");
                        break;

                    case "2":
                        Console.WriteLine("=== Xoá khách hàng ===");
                        Console.Write("Nhập số CMND khách hàng cần xoá: ");
                        int soCMNDXoa = Utility.Validate.GetValidNumberInput();
                        khachSan.XoaKhachHang(soCMNDXoa);
                        break;

                    case "3":
                        Console.WriteLine("=== Tính tiền thuê phòng ===");
                        Console.Write("Nhập số CMND khách hàng: ");
                        int soCMNDTinhTien = Utility.Validate.GetValidNumberInput();
                        Console.Write("Nhập số ngày thuê: ");
                        int soNgayThue = Utility.Validate.GetValidNumberInput();

                        decimal tienThuePhong = khachSan.TinhTienThuePhong(soCMNDTinhTien, soNgayThue);
                        if (tienThuePhong != 0)
                        {
                            Console.WriteLine("Tiền thuê phòng: $" + tienThuePhong);

                        }
                        else { Console.WriteLine("khách bùng kèo!"); }
                        break;

                    case "4":
                        Console.WriteLine("=== Danh sách khách hàng ===");
                        khachSan.HienThiDanhSachKhachHang();
                        break;

                    case "0":
                        ketThuc = true;
                        break;

                    default:
                        Console.WriteLine("Lựa chọn không hợp lệ.");
                        break;
                }

                Console.WriteLine("===========================");
            }
        }
    }
}