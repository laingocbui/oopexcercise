﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bai8
{
    internal class QuanLyThuVien
    {
        private List<TheMuon> theMuonList;

        public QuanLyThuVien()
        {
            theMuonList = new List<TheMuon>();
        }

        public void ThemPhieuMuon()
        {
            Console.WriteLine("\n===== Thêm phiếu mượn sách =====");

            TheMuon theMuon = new TheMuon();

            Console.Write("Nhập mã phiếu mượn: ");
            theMuon.MaPhieuMuon = Console.ReadLine();


            
            while (true)
            {
                Console.Write("Nhập ngày mượn: ");
                theMuon.NgayMuon = Console.ReadLine();

                if (Utility.Validate.IsValidBirthDate(theMuon.NgayMuon))
                {
                    break;
                }
                else
                {
                    Console.WriteLine($"{theMuon.NgayMuon} wrong format");
                }
            }



            Console.Write("Nhập hạn trả: ");
            theMuon.HanTra = Utility.Validate.GetValidNumberInput(); ;

            Console.Write("Nhập số hiệu sách: ");
            theMuon.SoHieuSach = Console.ReadLine();

            SinhVien sinhVienMuon = new SinhVien();

            Console.Write("Nhập mã sinh viên: ");
            sinhVienMuon.MaSV = Console.ReadLine();

            Console.Write("Nhập họ tên sinh viên: ");
            sinhVienMuon.HoTen = Console.ReadLine();

            Console.Write("Nhập tuổi sinh viên: ");
            sinhVienMuon.Tuoi = Utility.Validate.GetValidNumberInput(); ;

            Console.Write("Nhập lớp sinh viên: ");
            sinhVienMuon.Lop = Console.ReadLine();

            theMuon.SinhVienMuon = sinhVienMuon;

            theMuonList.Add(theMuon);

            Console.WriteLine("Phiếu mượn sách đã được thêm thành công.");
        }

        public void XoaPhieuMuon()
        {
            Console.WriteLine("\n===== Xoá phiếu mượn sách =====");

            Console.Write("Nhập mã phiếu mượn cần xoá: ");
            string maPhieuMuon = Console.ReadLine();

            TheMuon theMuon = theMuonList.Find(x => x.MaPhieuMuon == maPhieuMuon);
            if (theMuon != null)
            {
                theMuonList.Remove(theMuon);
                Console.WriteLine("Phiếu mượn sách đã được xoá thành công.");
            }
            else
            {
                Console.WriteLine("Không tìm thấy phiếu mượn sách với mã phiếu mượn đã nhập.");
            }
        }

        public void HienThiThongTinPhieuMuon()
        {
            Console.WriteLine("\n===== Thông tin phiếu mượn sách =====");

            if (theMuonList.Count == 0)
            {
                Console.WriteLine("Hiện không có phiếu mượn sách nào.");
                return;
            }

            foreach (var theMuon in theMuonList)
            {
                Console.WriteLine("Mã phiếu mượn: " + theMuon.MaPhieuMuon);
                Console.WriteLine("Ngày mượn: " + theMuon.NgayMuon);
                Console.WriteLine("Hạn trả: " + theMuon.HanTra);
                Console.WriteLine("Số hiệu sách: " + theMuon.SoHieuSach);
                Console.WriteLine("Mã sinh viên: " + theMuon.SinhVienMuon.MaSV);
                Console.WriteLine("Họ tên sinh viên: " + theMuon.SinhVienMuon.HoTen);
                Console.WriteLine("Tuổi sinh viên: " + theMuon.SinhVienMuon.Tuoi);
                Console.WriteLine("Lớp sinh viên: " + theMuon.SinhVienMuon.Lop);
                Console.WriteLine();
            }
        }
    }
}
