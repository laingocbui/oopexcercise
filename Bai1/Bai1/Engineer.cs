﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Bai1
{
    public class Engineer : Officer
    {
        public string Branch { get; set; }

        public Engineer(string name, int age, string gender, string address, string branch)
            : base(name, age, gender, address)
        {
            Branch = branch;
        }

        public override string ToString()
        {
            return $"Engineer{{branch='{Branch}', name='{Name}', age={Age}, gender='{Gender}', address='{Address}'}}";

        }
    }
}
