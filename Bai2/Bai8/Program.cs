﻿using System.Text;

namespace Bai8
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.UTF8;
            Console.WriteLine("===== Quản lý mượn trả sách =====");

            QuanLyThuVien QuanLyThuVien = new QuanLyThuVien();

            bool isRunning = true;
            while (isRunning)
            {
                Console.WriteLine("\nVui lòng chọn tác vụ:");
                Console.WriteLine("1. Thêm phiếu mượn sách");
                Console.WriteLine("2. Xoá phiếu mượn sách");
                Console.WriteLine("3. Hiển thị thông tin phiếu mượn sách");
                Console.WriteLine("4. Thoát");

                Console.Write("Lựa chọn của bạn: ");
                string choice = Console.ReadLine();

                switch (choice)
                {
                    case "1":
                        QuanLyThuVien.ThemPhieuMuon();
                        break;
                    case "2":
                        QuanLyThuVien.XoaPhieuMuon();
                        break;
                    case "3":
                        QuanLyThuVien.HienThiThongTinPhieuMuon();
                        break;
                    case "4":
                        isRunning = false;
                        break;
                    default:
                        Console.WriteLine("Lựa chọn không hợp lệ. Vui lòng chọn lại.");
                        break;
                }
            }

            Console.WriteLine("\nChương trình kết thúc.");
        }
    }
}