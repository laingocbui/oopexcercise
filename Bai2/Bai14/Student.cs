﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bai14
{
    public abstract class Student
    {
        public string FullName { get; set; }  
        public DateTime Dob { get; set; }  
        public string Gender { get; set; }  
        public string PhoneNumber { get; set; }  
        public string University { get; set; }  
        public string GradeLevel { get; set; }  
    }
}
