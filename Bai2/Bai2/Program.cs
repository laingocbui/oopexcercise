﻿using System.Text;

namespace Bai2
{
    internal class Program
    {
        static QuanLySach quanLySach = new QuanLySach();
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.UTF8;
            bool exit = false;

            while (!exit)
            {
                Console.WriteLine("----- CHƯƠNG TRÌNH QUẢN LÝ THƯ VIỆN -----");
                Console.WriteLine("1. Thêm mới tài liệu");
                Console.WriteLine("2. Xóa tài liệu");
                Console.WriteLine("3. Hiển thị thông tin tài liệu");
                Console.WriteLine("4. Tìm kiếm tài liệu theo loại");
                Console.WriteLine("5. Thoát");
                Console.Write("Vui lòng chọn chức năng (1-5): ");

                string choice = Console.ReadLine();
                Console.WriteLine();

                switch (choice)
                {
                    case "1":
                        ThemMoiTaiLieu();
                        break;
                    case "2":
                        XoaTaiLieu();
                        break;
                    case "3":
                        HienThiThongTinTaiLieu();
                        break;
                    case "4":
                        TimKiemTaiLieuTheoLoai();
                        break;
                    case "5":
                        exit = true;
                        break;
                    default:
                        Console.WriteLine("Lựa chọn không hợp lệ. Vui lòng chọn lại.\n");
                        break;
                }
            }
        }

        static void ThemMoiTaiLieu()
        {
            Console.WriteLine("----- THÊM MỚI TÀI LIỆU -----");
            Console.WriteLine("1. Thêm sách");
            Console.WriteLine("2. Thêm tạp chí");
            Console.WriteLine("3. Thêm báo");
            Console.Write("Vui lòng chọn loại tài liệu để thêm (1-3): ");

            string choice = Console.ReadLine();
            Console.WriteLine();

            switch (choice)
            {
                case "1":
                    ThemMoiSach();
                    break;
                case "2":
                    ThemMoiTapChi();
                    break;
                case "3":
                    ThemMoiBao();
                    break;
                default:
                    Console.WriteLine("Lựa chọn không hợp lệ. Quay lại menu chính.\n");
                    break;
            }
        }

        static void ThemMoiSach()
        {
            Console.WriteLine("----- THÊM MỚI SÁCH -----");
            Console.Write("Nhập mã tài liệu: ");
            string maTaiLieu = Console.ReadLine();
            Console.Write("Nhập tên nhà xuất bản: ");
            string tenNhaXuatBan = Console.ReadLine();
            Console.Write("Nhập số bản phát hành: ");
            int soBanPhatHanh = Utility.Validate.GetValidNumberInput();
            Console.Write("Nhập tên tác giả: ");
            string tenTacGia = Console.ReadLine();
            Console.Write("Nhập số trang: ");
            int soTrang = Utility.Validate.GetValidNumberInput();

            Sach sach = new Sach
            {
                MaTaiLieu = maTaiLieu,
                TenNhaXuatBan = tenNhaXuatBan,
                SoBanPhatHanh = soBanPhatHanh,
                TenTacGia = tenTacGia,
                SoTrang = soTrang
            };

            quanLySach.ThemMoiTaiLieu(sach);
            Console.WriteLine("Đã thêm sách thành công!\n");
        }

        static void ThemMoiTapChi()
        {
            Console.WriteLine("----- THÊM MỚI TẠP CHÍ -----");
            Console.Write("Nhập mã tài liệu: ");
            string maTaiLieu = Console.ReadLine();
            Console.Write("Nhập tên nhà xuất bản: ");
            string tenNhaXuatBan = Console.ReadLine();
            Console.Write("Nhập số bản phát hành: ");
            int soBanPhatHanh = Utility.Validate.GetValidNumberInput();
            Console.Write("Nhập số phát hành: ");
            int soPhatHanh = Utility.Validate.GetValidNumberInput();
            Console.Write("Nhập tháng phát hành: ");
            int thangPhatHanh = Utility.Validate.GetValidNumberInput();

            TapChi tapChi = new TapChi
            {
                MaTaiLieu = maTaiLieu,
                TenNhaXuatBan = tenNhaXuatBan,
                SoBanPhatHanh = soBanPhatHanh,
                SoPhatHanh = soPhatHanh,
                ThangPhatHanh = thangPhatHanh
            };

            quanLySach.ThemMoiTaiLieu(tapChi);
            Console.WriteLine("Đã thêm tạp chí thành công!\n");
        }

        static void ThemMoiBao()
        {
            Console.WriteLine("----- THÊM MỚI BÁO -----");
            Console.Write("Nhập mã tài liệu: ");
            string maTaiLieu = Console.ReadLine();
            Console.Write("Nhập tên nhà xuất bản: ");
            string tenNhaXuatBan = Console.ReadLine();
            Console.Write("Nhập số bản phát hành: ");
            int soBanPhatHanh = Utility.Validate.GetValidNumberInput();
            Console.Write("Nhập ngày phát hành (dạng dd/MM/yyyy): ");
            DateTime ngayPhatHanh = DateTime.Parse(Console.ReadLine());

            Bao bao = new Bao
            {
                MaTaiLieu = maTaiLieu,
                TenNhaXuatBan = tenNhaXuatBan,
                SoBanPhatHanh = soBanPhatHanh,
                NgayPhatHanh = ngayPhatHanh
            };

            quanLySach.ThemMoiTaiLieu(bao);
            Console.WriteLine("Đã thêm báo thành công!\n");
        }

        static void XoaTaiLieu()
        {
            Console.WriteLine("----- XÓA TÀI LIỆU -----");
            Console.Write("Nhập mã tài liệu cần xóa: ");
            string maTaiLieu = Console.ReadLine();

            quanLySach.XoaTaiLieu(maTaiLieu);
           // Console.WriteLine("Đã xóa tài liệu thành công!");
        }

        static void HienThiThongTinTaiLieu()
        {
            Console.WriteLine("----- HIỂN THỊ THÔNG TIN TÀI LIỆU -----");
            quanLySach.HienThiThongTinTaiLieu();
            Console.WriteLine();
        }

        static void TimKiemTaiLieuTheoLoai()
        {
            Console.WriteLine("----- TÌM KIẾM TÀI LIỆU THEO LOẠI -----");
            Console.WriteLine("1. Tìm kiếm sách");
            Console.WriteLine("2. Tìm kiếm tạp chí");
            Console.WriteLine("3. Tìm kiếm báo");
            Console.Write("Vui lòng chọn loại tài liệu để tìm kiếm (1-3): ");

            string choice = Console.ReadLine();
            Console.WriteLine();

            switch (choice)
            {
                case "1":
                    quanLySach.TimKiemTaiLieuTheoLoai("Sach");
                    break;
                case "2":
                    quanLySach.TimKiemTaiLieuTheoLoai("TapChi");
                    break;
                case "3":
                    quanLySach.TimKiemTaiLieuTheoLoai("Bao");
                    break;
                default:
                    Console.WriteLine("Lựa chọn không hợp lệ. Quay lại menu chính.\n");
                    break;
            }
        }
    }
}