﻿using System.Text.RegularExpressions;

namespace Utility
{
    public static class Validate
    {
        public static bool IsValidPhoneNumber(string phoneNumber)
        {

            string pattern = @"^0\d{9}$";
            return Regex.IsMatch(phoneNumber, pattern);
        }

        public static bool IsValidEmail(string email)
        {

            string pattern = @"^[A-Za-z0-9]+[A-Za-z0-9._-]*@[A-Za-z0-9-]+\.[A-Za-z0-9.-]+$";
            return Regex.IsMatch(email, pattern);
        }

        public static bool IsValidBirthDate(string birthDate)
        {

            string pattern = @"^(0[1-9]|[12][0-9]|3[01])/(0[1-9]|1[0-2])/((19|20)\d{2})$";
            return Regex.IsMatch(birthDate, pattern);
        }

        public static int GetValidNumberInput()
        {
            int number;

            while (true)
            {

                string input = Console.ReadLine();

                if (int.TryParse(input, out number))
                {
                    break;
                }
                else
                {
                    Console.Write("Please Enter Number: ");
                }
            }

            return number;
        }

        
    }
}