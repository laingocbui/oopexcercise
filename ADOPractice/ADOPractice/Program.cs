﻿using System.Data;
using System.Data.SqlClient;

namespace ADOPractice
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //string con = "Server=.;Database=MagicVilla;Trusted_Connection=True;TrustServerCertificate=True;MultipleActiveResultSets=True";

            //using (SqlConnection connection = new SqlConnection(con))
            //{
            //    try
            //    {
            //        connection.Open();
            //        string query1 = "select Id, Name, Details from Villa ";
            //        SqlDataAdapter dataAdapter = new SqlDataAdapter(query1, connection);
            //        DataTable dataTable = new DataTable();
            //        dataAdapter.Fill(dataTable);
            //        foreach (DataRow row in dataTable.Rows)
            //        {
            //            Console.WriteLine($"{row["Id"]} - {row["Name"]} - {row["Details"]}");
            //        }

            //        string insertQuery = "Insert into Villa( Name, Details, [UpdatedDate], Rate, Sqft, Occupancy,[CreatedDate]) values ('TestADO', 'Something', GetDate(), 200, 1000, 5,GetDate())";
            //        SqlCommand sqlInsertCommand = new SqlCommand(insertQuery, connection);
            //       // sqlInsertCommand.ExecuteNonQuery();

            //        string deleteQuery = "Delete from Villa where Id IN (21,28,29)";
            //        SqlCommand deleteSqlCommand = new SqlCommand(deleteQuery, connection);
            //        //deleteSqlCommand.ExecuteNonQuery();
            //        using SqlTransaction sqlTransaction = connection.BeginTransaction();
            //        try
            //        {
            //            sqlInsertCommand.Transaction = sqlTransaction;
            //            deleteSqlCommand.Transaction = sqlTransaction;
            //            sqlInsertCommand.ExecuteNonQuery ();
            //            deleteSqlCommand.ExecuteNonQuery () ;
            //            sqlTransaction.Commit ();
            //            Console.WriteLine("thanh cong");
            //        }
            //        catch (Exception)
            //        {
            //            Console.WriteLine("that bai");
            //            sqlTransaction.Rollback ();
            //        }
            //    }
            //    catch (Exception)
            //    {
            //        Console.WriteLine("lỗi");

            //    }
            //    finally
            //    {
            //        connection.Close();
            //    }
            //};

            string con = "Server=.;Database=MagicVilla;Trusted_Connection=True;TrustServerCertificate=True;MultipleActiveResultSets=True";
            SqlConnection connection = new SqlConnection(con);
            connection.Open();

            string cmd = "select * from Villa";
            string sp = "exec proc";
            SqlCommand command = new SqlCommand(cmd, connection);
            //command.CommandType = CommandType.StoredProcedure;
            
            SqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                var id = reader.GetInt32("ID");
                string name = (string)(reader["Name"]);
                Console.WriteLine($"{id} - {name}");
            }
            string deleteCmd = "Delete from Villa where Id =@Id";
            SqlCommand deleteCommand = new SqlCommand(deleteCmd, connection);
            deleteCommand.Parameters.AddWithValue("@Id", 35);
            deleteCommand.ExecuteNonQuery();

            DataSet dataSet = new DataSet();
            SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(cmd, connection);
            sqlDataAdapter.Fill(dataSet, "Villa");

            DataTable dataTable = new DataTable();
            dataTable = dataSet.Tables["Villa"];

            
            connection.Close();


            // kết nối đóng nhưng cái này lưu trên bộ nhớ chứ ko phải lấy từ db ra nên vẫn dùng đc
            foreach (DataRow row in dataTable.Rows)
            {
                int villaId = (int)row["Id"];
                string villaName = (string)row["Name"];
                Console.WriteLine($"{villaId} - {villaName}");
            }


            // kết nối đóng nên cái này ko dùng đc
            //while (reader.Read())
            //{
            //    Console.WriteLine( "==============");
            //    int id = (int)(reader["ID"]);
            //    string name = (string)(reader["Name"]);
            //    Console.WriteLine($"{id} - {name}");
            //}



        }
    }
}