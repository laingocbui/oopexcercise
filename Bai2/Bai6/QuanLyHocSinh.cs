﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bai6
{
    internal class QuanLyHocSinh
    {
        private List<HocSinh> _sinh;
        public QuanLyHocSinh() {
            _sinh = new List<HocSinh>();
        }

        public void ThemHocSinh(HocSinh hocsinh)
        {
            _sinh.Add(hocsinh);
        }
        public void HienThiHocSinh()
        {
            Console.WriteLine("Danh sách học sinh:");
            foreach (HocSinh hocSinh  in _sinh)
            {
                Console.WriteLine("Họ tên: " + hocSinh.HoTen);
                Console.WriteLine("Tuổi: " + hocSinh.Tuoi);
                Console.WriteLine("Quê quán: " + hocSinh.QueQuan);
              
                Console.WriteLine("==============================");
            }
        }
        public void HienThiHocSinh20()
        {
            var hocSinh20Tuoi = _sinh.Where(hs => hs.Tuoi == 20).ToList();
            if (hocSinh20Tuoi.Count !=0)
            {
                foreach (var hs in hocSinh20Tuoi)
                {
                    Console.WriteLine(hs.HoTen);
                }
            }
            else
            {
                Console.WriteLine("Không tìm thấy học sinh phù hợp!");
            }
            
        }

        public void DemHocSinh()
        {
            var soHocSinh = _sinh.Count(hs=>hs.Tuoi == 23 && hs.QueQuan == "DN");
            Console.WriteLine($"Số học sinh trên 23 tuổi và quê DN: {soHocSinh}");
        }
    }
}
