﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bai4
{
    public class HoGiaDinh
    {
        public List<Nguoi> NguoiList { get; set; }
        public int SoThanhVien { get; set; }
        public int SoNha { get; set; }
        public HoGiaDinh()
        {
            
                NguoiList = new List<Nguoi>();
        }

        public bool KiemTraSoCMNDDuyNhat()
        {
            HashSet<int> soCMNDSet = new HashSet<int>();

            foreach (Nguoi nguoi in NguoiList)
            {
                if (soCMNDSet.Contains(nguoi.CMND))
                {
                    return false; 
                }
                else
                {
                    soCMNDSet.Add(nguoi.CMND);
                }
            }

            return true; 
        }
    }
}
