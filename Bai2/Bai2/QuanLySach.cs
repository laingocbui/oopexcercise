﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bai2
{
    public class QuanLySach
    {
        private List<TaiLieu> danhSachTaiLieu;
        private HashSet<string> maTaiLieuList = new HashSet<string>();
        public QuanLySach()
        {
            danhSachTaiLieu = new List<TaiLieu>();
        }

        public void ThemMoiTaiLieu(TaiLieu taiLieu)
        {
            if (maTaiLieuList.Contains(taiLieu.MaTaiLieu))
            {
                Console.WriteLine("Mã tài liệu đã tồn tại. Không thể thêm tài liệu mới.\n");
                return;
            }

            maTaiLieuList.Add(taiLieu.MaTaiLieu);
            
            danhSachTaiLieu.Add(taiLieu);
        }

        public void XoaTaiLieu(string maTaiLieu)
        {
            TaiLieu taiLieuCanXoa = danhSachTaiLieu.Find(tl => tl.MaTaiLieu == maTaiLieu);
            if (taiLieuCanXoa != null)
            {
                danhSachTaiLieu.Remove(taiLieuCanXoa);
                Console.WriteLine("Đã xoá tài liệu có mã {0}", maTaiLieu);
            }
            else
            {
                Console.WriteLine("Không tìm thấy tài liệu có mã {0}", maTaiLieu);
            }
        }

        public void HienThiThongTinTaiLieu()
        {
            Console.WriteLine("Danh sách tài liệu trong thư viện:");
            foreach (TaiLieu taiLieu in danhSachTaiLieu)
            {
                Console.WriteLine("Mã tài liệu: {0}", taiLieu.MaTaiLieu);
                Console.WriteLine("Tên nhà xuất bản: {0}", taiLieu.TenNhaXuatBan);
                Console.WriteLine("Số bản phát hành: {0}", taiLieu.SoBanPhatHanh);

                if (taiLieu is Sach)
                {
                    Sach sach = taiLieu as Sach;
                    Console.WriteLine("Tên tác giả: {0}", sach.TenTacGia);
                    Console.WriteLine("Số trang: {0}", sach.SoTrang);
                }
                else if (taiLieu is TapChi)
                {
                    TapChi tapChi = taiLieu as TapChi;
                    Console.WriteLine("Số phát hành: {0}", tapChi.SoPhatHanh);
                    Console.WriteLine("Tháng phát hành: {0}", tapChi.ThangPhatHanh);
                }
                else if (taiLieu is Bao)
                {
                    Bao bao = taiLieu as Bao;
                    Console.WriteLine("Ngày phát hành: {0}", bao.NgayPhatHanh.ToShortDateString());
                }

                Console.WriteLine();
            }
        }

        public void TimKiemTaiLieuTheoLoai(string loaiTaiLieu)
        {
            Console.WriteLine("Danh sách tài liệu loại {0}:", loaiTaiLieu);

            foreach (TaiLieu taiLieu in danhSachTaiLieu)
            {
                if (taiLieu.GetType().Name.Equals(loaiTaiLieu, StringComparison.OrdinalIgnoreCase))
                {
                    Console.WriteLine("Mã tài liệu: {0}", taiLieu.MaTaiLieu);
                    Console.WriteLine("Tên nhà xuất bản: {0}", taiLieu.TenNhaXuatBan);
                    Console.WriteLine("Số bản phát hành: {0}", taiLieu.SoBanPhatHanh);

                    if (taiLieu is Sach)
                    {
                        Sach sach = taiLieu as Sach;
                        Console.WriteLine("Tên tác giả: {0}", sach.TenTacGia);
                        Console.WriteLine("Số trang: {0}", sach.SoTrang);
                    }
                    else if (taiLieu is TapChi)
                    {
                        TapChi tapChi = taiLieu as TapChi;
                        Console.WriteLine("Số phát hành: {0}", tapChi.SoPhatHanh);
                        Console.WriteLine("Tháng phát hành: {0}", tapChi.ThangPhatHanh);
                    }
                    else if (taiLieu is Bao)
                    {
                        Bao bao = taiLieu as Bao;
                        Console.WriteLine("Ngày phát hành: {0}", bao.NgayPhatHanh.ToShortDateString());
                    }

                    Console.WriteLine();
                }
            }
        }
    }
}
