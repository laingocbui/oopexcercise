﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Net.Mime.MediaTypeNames;

namespace Bai10
{
    internal class VanBan
    {
        public string  Text { get; set; }
        public VanBan()
        {
            Text = "";
        }

        public VanBan(string st)
        {
            Text = st;
        }

        public int DemSoTu()
        {
            string[] words = Text.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            return words.Length;
        }

        public int DemSoLuongKyTuA()
        {
            int count = 0;
            string uppercaseText = Text.ToUpper();
            foreach (char c in uppercaseText)
            {
                if (c == 'A')
                    count++;
            }
            return count;
        }

        public void ChuanHoaVanBan()
        {
            Text = Text.Trim();
            while (Text.Contains("  "))
            {
                Text = Text.Replace("  ", " ");
            }
        }
    }
}
