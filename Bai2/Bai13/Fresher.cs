﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bai13
{
    internal class Fresher : Employee
    {
      
        public DateTime Graduation_date { get; set; }
        public string Graduation_rank { get; set; }
        public string Education { get; set; }

  
        public Fresher(string id, string fullName, DateTime birthDay, string phone, string email, List<Certificate> certificates, DateTime graduation_date, string graduation_rank, string education)
            : base(id, fullName, birthDay, phone, email,certificates)
        {
            Graduation_date = graduation_date;
            Graduation_rank = graduation_rank;
            Education = education;
            Employee_type = 1;
            Employee_count++;
        }

       
        public override void ShowInfo()
        {
            Console.WriteLine($"ID: {ID}");
            Console.WriteLine($"Full Name: {FullName}");
            Console.WriteLine($"Birth Day: {BirthDay.ToShortDateString()}");
            Console.WriteLine($"Phone: {Phone}");
            Console.WriteLine($"Email: {Email}");
            Console.WriteLine($"Employee Type: Fresher");
            Console.WriteLine($"Graduation Date: {Graduation_date.ToShortDateString()}");
            Console.WriteLine($"Graduation Rank: {Graduation_rank}");
            Console.WriteLine($"Education: {Education}");
            Console.WriteLine("=======================");

            foreach (Certificate certificate in Certificates)
            {
                Console.WriteLine($"Certificate ID: {certificate.CertificateID}");
                Console.WriteLine($"Certificate Name: {certificate.CertificateName}");
                Console.WriteLine($"Certificate Rank: {certificate.CertificateRank}");
                Console.WriteLine($"Certificate Date: {certificate.CertificateDate}");
                Console.WriteLine("=======================");
            }
        }
    }
}
