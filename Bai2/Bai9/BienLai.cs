﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bai9
{
    internal class BienLai
    {
        public KhachHang KhachHang { get; set; }
        public int ChiSoCu { get; set; }
        public int ChiSoMoi { get; set; }

        public int TinhTienDien()
        {
            return (ChiSoMoi - ChiSoCu) * 5;
        }
    }
}
